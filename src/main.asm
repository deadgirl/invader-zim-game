.include "include/data.asm"
.include "include/library.asm"

.intel_syntax noprefix
.data

argcheck1:
	.asciz "story"

argcheck2:
	.asciz "-s"

stats:
	.quad 1000	# Miles left
	.quad 100 	# Voot Cruiser charge (%)
	.quad 100 	# Health (%)
	.quad 30 	# Amount of food (lbs)

.text
.global _start

_start:
	pop 	rbx 	# Argument count
	cmp 	rbx, 2
	jl 		main

	# pop two times for first real argument
	pop 	rbx
	pop 	rbx
	lea 	rcx, argcheck1
	call 	cmpstr

	cmp 	rax, 0 		# rax = return address from cmpsr(). 0 = true, 1 = false
	je 		print_story

	lea 	rcx, argcheck2
	call 	cmpstr

	cmp 	rax, 1 		# rax = return address from cmpsr(). 0 = true, 1 = false
	je 		main
print_story:
	lea 	rax, story
	call 	prntstr

	mov 	rax, 60 	# exit() syscall
	mov 	rdi, 0 		# return code 0 (no errors)
	syscall
main:
	call 	ClearScreen
	mov 	rbx, 1 		# rbx = current week
	lea 	rax, header
	call 	prntstr

# Start gameloop

gameloop:
	cmp 	rbx, 24 	# Game ends after 24 weeks
	jg  	end_timeout

	# Print current week
	lea 	rax, week1
	call 	prntstr
	mov 	rax, rbx
	call 	PrintInteger
	lea 	rax, week2
	call 	prntstr

	# User prompt
	call 	prntstatus
	lea 	rax, prompt
	call 	prntstr
	call 	ScanInteger	# rba = user input
	call 	ClearScreen
switch1:
	cmp 	rax, 1
	jne 	switch2

	# Checks Voot Cruiser charge
	mov 	rcx, [stats + 8 * 1]
	cmp 	rcx, 0
	jg 		switch1_cont1

	# Runs if Voot Cruiser has no charge
	lea 	rax, nomove
	call 	prntstr
	jmp 	gameloop
switch1_cont1:
	# Get range of 25-100
	mov 	rax, 76
	call 	GetRandom
	add 	rax, 25
	push 	rax			# Push on stack for later use

	# Subtracts miles and checks if miles left are less than/equal to 0.
	mov 	rcx, [stats + 8 * 0]
	sub 	rcx, rax
	mov 	[stats + 8 * 0], rcx
	cmp 	rcx, 0
	jle 	end_win 	# Ends game

	lea 	rax, advanced1
	call 	prntstr
	pop 	rax 		# The number of miles traveled
	call 	PrintInteger
	lea 	rax, advanced2
	call 	prntstr

	# Get range of 1-30
	mov 	rax, 30
	call	GetRandom
	inc 	rax
	push 	rax 		# Push on stack for later use

	# Subtracts from Voot Cruiser charge.
	mov 	rcx, [stats + 8 * 1]
	sub	 	rcx, rax
	cmp 	rcx, 0
	jge 	switch1_cont2

	# Charge is set to 0 if negative
	mov 	rcx, 0
switch1_cont2:
	mov 	[stats + 8 * 1], rcx

	lea 	rax, cost1
	call 	prntstr
	pop 	rax 	# The amount of charge used
	call 	PrintInteger
	lea 	rax, cost2
	call 	prntstr

	jmp 	switch_end
switch2:
	cmp 	rax, 2
	jne 	switch3

	# Checks Voot Cruiser charge
	mov 	rcx, [stats + 8 * 1]
	cmp 	rcx, 0
	jg 		switch2_cont1

	lea 	rax, nomove
	call 	prntstr
	jmp 	gameloop
switch2_cont1:
	# Get range of 10-50
	mov 	rax, 41
	call 	GetRandom
	add 	rax, 10
	push 	rax 		# Push for later use

	# Adds pounds of food
	mov 	rcx, [stats + 8 * 3]
	add 	rcx, rax
	mov 	[stats + 8 * 3], rcx

	lea 	rax, food1
	call	prntstr
	pop 	rax
	call 	PrintInteger
	lea 	rax, food2
	call 	prntstr

	# Get range of 1-30
	mov 	rax, 30
	call	GetRandom
	inc 	rax
	push 	rax 		# Push on stack for later use

	# Subtracts from Voot Cruiser charge.
	mov 	rcx, [stats + 8 * 1]
	sub	 	rcx, rax
	cmp 	rcx, 0
	jge 	switch2_cont2

	# Charge is set to 0 if negative
	mov 	rcx, 0
switch2_cont2:
	mov 	[stats + 8 * 1], rcx

	lea 	rax, cost1
	call 	prntstr
	pop 	rax 	# The amount of charge used
	call 	PrintInteger
	lea 	rax, cost2
	call 	prntstr

	jmp 	switch_end
switch3:
	cmp 	rax, 3
	jne 	switch4

	# Get range of 10-50
	mov 	rax, 41
	call	GetRandom
	add 	rax, 10
	push 	rax 		# Push on stack for later use

	# Add charge to Voot Cruiser
	mov 	rcx, [stats + 8 * 1]
	add 	rcx, rax
	cmp 	rcx, 100
	jle 	switch3_cont

	mov 	rcx, 100
switch3_cont:
	mov 	[stats + 8 * 1], rcx

	# Print amount of added charge
	lea 	rax, charge1
	call 	prntstr
	pop 	rax		# The amoung of added charge
	call 	PrintInteger
	lea 	rax, charge2
	call 	prntstr

	jmp 	switch_end
switch4:
	cmp 	rax, 4
	jne 	switch_def

	# Exit program
	lea 	rax, sd
	call 	prntstr

	mov 	rax, 60 	# exit() syscall
	mov 	rdi, 0 		# return code 0 (no errors)
	syscall
switch_def:
	lea 	rax, invalid
	call 	prntstr
	jmp 	gameloop
switch_end:
	# Hunger system
	mov 	rcx, [stats + 8 * 3]
	cmp 	rcx, 0
	jg 		switch_end_cont1

	# Get range of 10-50
	mov 	rax, 41
	call	GetRandom
	add 	rax, 10
	push 	rax 		# Push on stack for later use

	mov 	rcx, [stats + 8 * 2]
	sub 	rcx, rax
	mov 	[stats + 8 * 2], rcx
	cmp 	rcx, 0
	jle 	end_starve

	lea 	rax, health1
	call 	prntstr
	pop 	rax 		# The amount of health lost due to hunger
	call 	PrintInteger
	lea 	rax, health2
	call 	prntstr
switch_end_cont1:
	# Get range of 1-10
	mov 	rax, 10
	call	GetRandom
	inc 	rax

	# Deplete food
	mov 	rcx, [stats + 8 * 3]
	sub 	rcx, rax
	cmp 	rcx, 0
	jge 	switch_end_cont3

	mov 	rcx, 0
switch_end_cont3:
	mov 	[stats + 8 * 3], rcx
	inc 	rbx 		# Increase week counter
	jmp 	gameloop

# End gameloop

end_win:
	lea 	rax, victory
	call 	prntstr

	mov 	rax, 60 	# exit() syscall
	mov 	rdi, 0 		# return code 0 (no errors)
	syscall
end_timeout:
	lea 	rax, time
	call 	prntstr

	mov 	rax, 60 	# exit() syscall
	mov 	rdi, 1 		# return code 1 (failed)
	syscall
end_starve:
	lea 	rax, starve
	call 	prntstr

	mov 	rax, 60 	# exit() syscall
	mov 	rdi, 1 		# return code 1 (failed)
	syscall
