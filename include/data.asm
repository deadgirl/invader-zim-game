.data

header:
	.ascii "=================================\n"
	.ascii "Invader Zim: The Nightmare Begins\n"
	.ascii "=================================\n\n"
	.asciz "For the full story, self destruct (option 4) and rerun with \"-s\" or \"story\"\n\n"

story:
	.ascii "=================================\n"
	.ascii "Invader Zim: The Nightmare Begins\n"
	.ascii "=================================\n\n"
	.ascii "As a part of the Irken Armada's newest plan\nfor universal domination, Operation Impending Doom II,\nwe follow our hero, Invader Zim, as he sets out on a\ncourageous expedition to conquer the floating dirtball known as \"Earth.\"\n\n"
	.ascii "Despite the apparent importance of his\nmission, the Almighty Tallest, the leaders of the Irken\nrace, don't know that Earth actually exists, and are just\ntrying to get Zim out of the way after he ruined Operation Impending Doom I.\n\n"
	.asciz "Before he can destroy the puny meatbags\n(humans), Zim must make the arduous adventure through space\nwith his trusty Voot Cruiser.\n\nGood luck, brave Invader Zim!\n"

week1:
	.ascii "==========\n"
	.asciz "Week "

week2:
	.ascii "/24\n"
	.asciz "==========\n"

prompt:
	.ascii "Next move:\n"
	.ascii "1.) Venture forth\n"
	.ascii "2.) Search for sustenance\n"
	.ascii "3.) Charge the Voot Cruiser\n"
	.ascii "4.) Self destruct\n"
	.asciz "> "

nomove:
	.asciz "The Voot Cruiser is out of juice!\n\n"

advanced1:
	.asciz "You have advanced "

advanced2:
	.asciz " miles\n\n"

cost1:
	.asciz "You used "

cost2:
	.asciz "% of the Cruiser's charge.\n\n"

food1:
	.asciz "You found "

food2:
	.asciz " food things!\n"

charge1:
	.asciz "The Voot is "

charge2:
	.asciz "% more charged.\n\n"

health1:
	.asciz "You have lost "

health2:
	.asciz "hp\n\n"

statusD:
	.asciz "Distance to earth:\t"

statusV:
	.asciz "Voot Runner Charge:\t"

statusZ:
	.asciz "Zim's Health:\t\t"

statusF1:
	.asciz "Food:\t\t\t"

statusF2:
	.asciz "lbs\n"

sd:
	.asciz "Self destruction sequence engaged\n\nGAME OVER\n"

invalid:
	.asciz "Invalid selection\n\n"

victory:
	.asciz "VICTORY IS MINE!\n\nIt is now time to prepare for the Tallests' arrival...\n\nYOU WIN\n"

time:
	.asciz "You didn't make it in time.\n\nGAME OVER\n"	

starve:
	.asciz "You starved to death.\n\nGAME OVER\n"

prc:
	.asciz "%\n"

nl:
	.asciz "\n"
