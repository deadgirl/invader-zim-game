.intel_syntax noprefix
.text

cmpstr:
	push 	rdi
	push 	rdx

	call 	strleng		# Populates rdx with string length
	mov 	rdi, -1
	mov 	rax, 0
cmpstr_loop:
	inc 	rdi
	cmp 	rdi, rdx
	jge 	cmpstr_end

	push 	rdx

	movb 	dh, [rbx + rdi]
	movb 	dl, [rcx + rdi]

	cmpb 	dh, dl
	pop 	rdx
	je 		cmpstr_loop

	mov 	rax, 1 		# 1 = strings do not match
cmpstr_end:
	pop 	rdx
	pop 	rdi
	ret

prntstr:
	push 	rax
	push 	rbx
	push 	rdi
	push 	rsi
	push 	rdx

	mov 	rbx, rax

	mov 	rax, 1 		# write() syscall
	mov 	rdi, 1 		# stdout
	mov 	rsi, rbx
	call 	strleng 	# Populates rdx with string length

	syscall

	pop 	rdx
	pop 	rsi
	pop 	rdi
	pop 	rbx
	pop 	rax
	ret

strleng:
	mov 	rdx, 0
strleng_loop:
	cmpb 	[rbx + rdx], 0
	je 		strleng_end

	inc 	rdx
	jmp 	strleng_loop
strleng_end:
	ret

prntstatus:
	push 	rax

	lea 	rax, statusD
	call 	prntstr
	mov 	rax, [stats + 8 * 0]
	call 	PrintInteger
	call	prntnl

	lea 	rax, statusV
	call 	prntstr
	mov 	rax, [stats + 8 * 1]
	call 	PrintInteger
	lea 	rax, prc
	call 	prntstr

	lea 	rax, statusZ
	call 	prntstr
	mov 	rax, [stats + 8 * 2]
	call 	PrintInteger
	lea 	rax, prc
	call 	prntstr

	lea 	rax, statusF1
	call 	prntstr
	mov 	rax, [stats + 8 * 3]
	call 	PrintInteger
	lea 	rax, statusF2
	call 	prntstr
	call 	prntnl

	pop 	rax
	ret

prntnl:
	push 	rax
	lea 	rax, nl
	call 	prntstr
	pop 	rax
	ret
